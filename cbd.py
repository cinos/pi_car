# -*- coding: utf-8 -*-
from pismart.amateur import PiSmart
from time import sleep
import random
from pismart.led import LED
from LogisticRegression import LogReg

leds = LED()
leds.DEBUG = 'debug'

def led_at_start_up():
    LED_MAX = 100
    LED_MIN = 0

    for i in range(3):
        for x in xrange(0, 100, 1):
            leds.brightness = x
            sleep(0.001)
            
        for x in xrange(100, -1, -1):
            leds.brightness = x
            sleep(0.001)
    
def setup():
    global pi
    pi = PiSmart()
    # print 'hi' # begin CLI
    print("*******************************************************")
    print("*             PI - THE AI POWERED MACHINE             *")
    print("*                (c) 2018 CBD Robotics                *")
    print("*******************************************************")
    led_at_start_up()
    pi.Say="hi, My name is pie" # writen "Pi", spoken "Pie"
    pi.Say="I am a demo robot from the CBD Robotics company"
    pi.MotorA = 80
    pi.MotorB = 80
    sleep(0.25)
    pi.MotorA = -80
    pi.MotorB = -80
    sleep(0.25)
    pi.MotorA = 0
    pi.MotorB = 0
    pi.Say="to begin, please say"
    pi.Say="hi pie"


class Ft:
    # FEATURES: dict of features (as key) and unique values of that feature (as value)
    FEATURES = dict()
    FEATURES["contract"] =          ["month to month","one year","two year"]
    FEATURES["internet_service"] =  ["D S L","fiber optic","No"]
    FEATURES["tenure"] =            int()
    FEATURES["paperless_billing"] = ["No","Yes"]
    FEATURES["streaming_TV"] =      ["No","No internet service", "Yes"]
    FEATURES["payment_method"] =    ["Bank transfer (automatic)", "Credit card (automatic)", "Electronic check", "Mailed check"]
    FEATURES["monthly_charge"] =    int()
    FEATURES["streaming_movies"] =  ["No","No internet service", "Yes"]
    FEATURES["multiple_line"] =     ["No","No phone service", "Yes"]
    order = ['contract','internet_service','tenure','paperless_billing',
             'streaming_TV','payment_method','monthly_charge',
             'streaming_movies','multiple_line']
    user_input = dict() # stores user's variables for prediction
    is_1st_ft = True
    wrong_input = 0 # wrong input 5 times, raise suggestion
    
    
    def __init__(self,feature_str):
        # feature_str: feature name in string-with-"_" format
        # var_in_ft: list of unique values in one feature
        # ft_name: feature name with spaces
        self.feature_str = feature_str
        self.var_in_ft = Ft.FEATURES[feature_str]
        self.ft_name = (lambda x: x.replace("_"," "))(feature_str) # "_" replaced by blank spaces (for TTS)

    
    def suggest_value(self):
        # in-case customer forget which unique values in each feature, give suggestions
        if type(self.var_in_ft) is list:
            pi.Say = "The values of, " + self.ft_name + ", are"
            for var in self.var_in_ft:
                if var != self.var_in_ft[-1]:
                    pi.Say = var
                else:
                    pi.Say = ", and " + var
        elif type(self.var_in_ft) is int:
            pi.Say = "The values of, " + self.ft_name + ", are in numbers"
            pi.Say = "please say an integer, from one, to one hundred"


    def ask_for_input(self):
        if Ft.is_1st_ft:
            pi.Say = "First please tell me about the, " + self.ft_name +", of the customer"
            pi.Say = "If you forgot the value in each feature, please say"
            pi.Say = "suggest me"
            

        else:
            phrases = ["Now please tell me about the, " + self.ft_name,
                       "The next feature is, "+ self.ft_name+", please tell me about it",
                       "Please give me details about the, "+ self.ft_name+", of the customer"]
            pi.Say = random.choice(phrases)


    def heard_confirm(self, data):
        # give feedback if heard and saved input successfully
        phrases = ["ok", "ok","ok","ok",
                   "you have a nice voice, I heard it clearly",
                   "ok, I heard it",
                   "roger, loud and clear",
                   "thanks, I get it"]
        pi.Say = random.choice(phrases)

        if Ft.is_1st_ft:
            pi.Say = "Let me repeat what I have heard"
        
        pi.Say = "the feature name is " + self.ft_name
        pi.Say = "the value that I heard is " + str(data)


    def save_and_confirm(self, data):
        # save "data" to Ft.user_input then run heard_confirm()
        Ft.user_input[self.feature_str] = data
        self.heard_confirm(data)
        Ft.wrong_input = 0


    @staticmethod
    def show_help():
        pi.Say = "I think cannot hear you clearly"
        pi.Say = "If you need any help, please say, suggest me, again"
        Ft.wrong_input = 0
        

    def get_input(self):
        print "Pi: "+self.ft_name # for debugging
        if type(self.var_in_ft) is list:
            self.ask_for_input()
                
            while True:
                pi.listen
                print "me: "+pi.result # for debugging
                if pi.result in self.var_in_ft:
                    self.save_and_confirm(pi.result)
                    break
                elif pi.result == "suggest me":
                    self.suggest_value()
                else:
                    Ft.wrong_input+=1

                if Ft.wrong_input == 5:
                    Ft.show_help()
                    
        elif type(self.var_in_ft) is int:
            self.ask_for_input()
            
            while True:
                pi.listen
                print "me: "+pi.result # for debugging
                try:
                    int_rlt = int(pi.result) # convert result format to int
                except ValueError: # if the result is a text, try check "suggest me" command
                    if pi.result == "suggest me": 
                        self.suggest_value()
                    else:
                        Ft.wrong_input+=1
                else:
                    self.save_and_confirm(int_rlt) # if convert successfully, save int_rlt
                    break

                if Ft.wrong_input == 5:
                    Ft.show_help()

                
def listen():
    while True:
        print "I'm hearing"
        pi.listen # start hearing
        print pi.result # for debugging
        if pi.result == "hi pie": # if robot hear...
            pi.Say="Hi"
            pi.Say="I will start to record your voice to predict whether the customer will churn or not"
            break
        
        elif pi.result == "bye pie": # if robot hear...
            pi.Say="See you later"
            pi.end()
            break
        
    for i in Ft.order:
        ft = Ft(i)
        ft.get_input()
        if Ft.is_1st_ft:
            Ft.is_1st_ft= False
            
    print Ft.user_input # for debugging


def predict():
    model = LogReg(Ft.FEATURES)
    result = model.predict(Ft.user_input)
    print result # for debugging
    pi.Say = "And the result is"
    if result == "No":
        pi.Say = "No, This customer will not churn"
        print "No, This customer will not churn" # for debugging
    elif result == "Yes": #should not use "else" here
        pi.Say = "Yes, This customer will churn"
        print "Yes, This customer will churn" # for debugging

    pi.Say = "Thank you for asking me, I will start sleep mode"
    pi.Say = "to ask me about other customers, again, please say"
    pi.Say = "hi pie"

                                     
if __name__ == "__main__":
    try:
        setup()
        while True:
            listen()
            predict()
    except KeyboardInterrupt:
        end()

