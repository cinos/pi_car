# request audio_link using FPT TTS API
import requests

def stt():
    audio_path = "speech.mp3"
    files = {'speechtest': open(audio_path,'rb')}

    r = requests.post("https://api.openfpt.vn/fsr",
        headers={"api_key": "d3ea11e6fe5741c0ae17b7691614c9de"},
        files=files)
    
    text = r.json()['hypotheses'][0]['utterance']
    return text

if __name__ = "__main__":
    t = stt()
    print t
    print type(t)
    print("done")
