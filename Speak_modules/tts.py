# -*- coding: utf-8 -*-
import requests
import playsound


def tts(text, debug = False):
    #text = text.encode('utf-8')
    r = requests.post(
        "http://api.openfpt.vn/text2speech/v4",
        headers={
            "api_key": "d3ea11e6fe5741c0ae17b7691614c9de",
            "speed": "1",
            "voice": "male",
            "prosody": "1",
            "Cache-Control": "no-cache"
        },
        data=text)
    audio_link = dict(r.json())['async']
    if debug:
        print(audio_link)

    # write/download the link to an mp3 file
    au = requests.get(audio_link, allow_redirects=True)
    open('speech.mp3', 'wb').write(au.content)

    # play audio file
    playsound.playsound('speech.mp3', True)
    
if __name__ == "__main__":
    tts("đây là một đoạn tiếng Việt mới toanh")
    print "done"
