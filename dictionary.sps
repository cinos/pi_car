[hi pie];
@results
    0 {'hi pie'}
@

[suggest me];
@results
    0 {'suggest me'}
@

[bye pie];
@results
    0 {'bye pie'}
@

[no internet service]|[no internet];
@results
    0 {'No internet service'}
    1 {'No internet service'}
@

[no phone service]|[no phone];
@results
    0 {'No phone service'}
    1 {'No phone service'}
@


[yes]|[no];
@results
    0 {'Yes'}
    1 {'No'}
@

[month to month]|[one year]|[two year];
@results
    0 {'month to month'}
    1 {'one year'}
    2 {'two year'}
@

[month]|[monthly];
@results
    0 {'month to month'}
    1 {'month to month'}
@

[fiber optic]|[d s l];
@results
    0 {'fiber optic'}
    1 {'D S L'}
@

[electronic check]|[mailed check]|[bank transfer]|[credit card];
@results
    0 {'Electronic check'}
    1 {'Mailed check'}
    2 {'Bank transfer (automatic)'}
    3 {'Credit card (automatic)'}
@

[one hundred];
@results
    0 {'100'}
@

[zero]|[one]|[two]|[three]|[four]|[five];
@results
    0 {'0'}
    1 {'1'}
    2 {'2'}
    3 {'3'}
    4 {'4'}
    5 {'5'}
@

[six]|[seven]|[eight]|[nine]|[ten];
@results
    0 {'6'}
    1 {'7'}
    2 {'8'}
    3 {'9'}
    4 {'10'}
@

[eleven]|[twelve]|[thirteen]|[fourteen]|[fifteen];
@results
    0 {'11'}
    1 {'12'}
    2 {'13'}
    3 {'14'}
    4 {'15'}
@

[sixteen]|[seventeen]|[eighteen]|[nineteen]|[twenty];
@results
    0 {'16'}
    1 {'17'}
    2 {'18'}
    3 {'19'}
    4 {'20'}
@

[twenty one]|[twenty two]|[twenty three]|[twenty four]|[twenty five];
@results
    0 {'21'}
    1 {'22'}
    2 {'23'}
    3 {'24'}
    4 {'25'}
@

[twenty six]|[twenty seven]|[twenty eight]|[twenty nine]|[thirty];
@results
    0 {'26'}
    1 {'27'}
    2 {'28'}
    3 {'29'}
    4 {'30'}
@

[thiry one]|[thiry two]|[thiry three]|[thiry four]|[thiry five];
@results
    0 {'31'}
    1 {'32'}
    2 {'33'}
    3 {'34'}
    4 {'35'}
@

[thiry six]|[thiry seven]|[thiry eight]|[thiry nine]|[forty];
@results
    0 {'36'}
    1 {'37'}
    2 {'38'}
    3 {'39'}
    4 {'40'}
@

[forty one]|[forty two]|[forty three]|[forty four]|[forty five];
@results
    0 {'41'}
    1 {'42'}
    2 {'43'}
    3 {'44'}
    4 {'45'}
@

[forty six]|[forty seven]|[forty eight]|[forty nine]|[fifty];
@results
    0 {'46'}
    1 {'47'}
    2 {'48'}
    3 {'49'}
    4 {'50'}
@

[fifty one]|[fifty two]|[fifty three]|[fifty four]|[fifty five];
@results
    0 {'51'}
    1 {'52'}
    2 {'53'}
    3 {'54'}
    4 {'55'}
@

[fifty six]|[fifty seven]|[fifty eight]|[fifty nine]|[sixty];
@results
    0 {'56'}
    1 {'57'}
    2 {'58'}
    3 {'59'}
    4 {'60'}
@

[sixty one]|[sixty two]|[sixty three]|[sixty four]|[sixty five];
@results
    0 {'61'}
    1 {'62'}
    2 {'63'}
    3 {'64'}
    4 {'65'}
@

[sixty six]|[sixty seven]|[sixty eight]|[sixty nine]|[seventy];
@results
    0 {'66'}
    1 {'67'}
    2 {'68'}
    3 {'69'}
    4 {'70'}
@

[seventy one]|[seventy two]|[seventy three]|[seventy four]|[seventy five];
@results
    0 {'71'}
    1 {'72'}
    2 {'73'}
    3 {'74'}
    4 {'75'}
@

[seventy six]|[seventy seven]|[seventy eight]|[seventy nine]|[eighty];
@results
    0 {'76'}
    1 {'77'}
    2 {'78'}
    3 {'79'}
    4 {'80'}
@
[eighty one]|[eighty two]|[eighty three]|[eighty four]|[eighty five];
@results
    0 {'81'}
    1 {'82'}
    2 {'83'}
    3 {'84'}
    4 {'85'}
@

[eighty six]|[eighty seven]|[eighty eight]|[eighty nine]|[ninety];
@results
    0 {'86'}
    1 {'87'}
    2 {'88'}
    3 {'89'}
    4 {'90'}
@

[ninety one]|[ninety two]|[ninety three]|[ninety four]|[ninety five];
@results
    0 {'91'}
    1 {'92'}
    2 {'93'}
    3 {'94'}
    4 {'95'}
@

[ninety six]|[ninety seven]|[ninety eight]|[ninety nine]|;
@results
    0 {'96'}
    1 {'97'}
    2 {'98'}
    3 {'99'}
    4 {'100'}
@