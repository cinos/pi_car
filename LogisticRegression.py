import numpy as np
import pandas as pd
import pickle


with open("lr.model","rb") as file:
    model = pickle.load(file)


class LogReg:
    feature_dict = dict()
    
    def __init__(self,feature_dict):
        LogReg.feature_dict = feature_dict

    @staticmethod
    def one_hot_encoder(value,array):
        result = []
        for i in array:
            result.append(1 if value == i else 0)
        return result


    @staticmethod
    def transform(raw_input):
        # raw_input: a dictionary
        # put numeric and encoded value in legit format for prediction
        print type(raw_input)
        temp_output = [raw_input['tenure'],
                           raw_input['monthly_charge']]
        list_to_encode = ['contract','internet_service','paperless_billing',
                          'streaming_TV','payment_method','streaming_movies',
                          'multiple_line']
        for i in list_to_encode:
            temp_output += LogReg.one_hot_encoder(raw_input[i], LogReg.feature_dict[i])

        finished_output = np.array(temp_output,dtype=np.float64)
        return finished_output

    
    def predict(self,raw_input):
        # get raw input, return whether the customer will churn or not
        # raw_input: a dictionary
        finished_input = LogReg.transform(raw_input)
        num_result = model.predict(finished_input) 
        if num_result[0]==0: # num_result is in np.array type
            return "No"
        else:
            return "Yes"
        
# testing zone
if __name__ == "__main__":
    
    od = dict()
    od['contract']='month to month'
    od['internet_service']='D S L'
    od['tenure']=2
    od['paperless_billing']='Yes'
    od['streaming_TV']='No'
    od['payment_method']='Bank transfer (automatic)'
    od['monthly_charge']=100
    od['streaming_movies']='No'
    od['multiple_line']='No'

    FEATURES = dict()
    FEATURES["contract"] =          ["month to month","one year","two year"]
    FEATURES["internet_service"] =  ["D S L","fiber optic","No"]
    FEATURES["tenure"] =            int()
    FEATURES["paperless_billing"] = ["No","Yes"]
    FEATURES["streaming_TV"] =      ["No","No internet service", "Yes"]
    FEATURES["payment_method"] =    ["Bank transfer (automatic)", "Credit card (automatic)", "Electronic check", "Mailed check"]
    FEATURES["monthly_charge"] =    int()
    FEATURES["streaming_movies"] =  ["No","No internet service", "Yes"]
    FEATURES["multiple_line"] =     ["No","No internet service", "Yes"]

    
    m = LogReg(FEATURES)
    result = m.predict(od)
    print result
